'use strict'

const pluginName = 'zoomfetch'

/**
 * Function always receieves a following object:
 *  { datasetIndex: number, element: Element, index: number }
 * 
 * @param {Object} startValues
 * @param {Object} endValues 
 */
function onZoom(startValues, endValues) {
    console.warn("onZoom: NOT IMPLEMENTED YET.")
}

/**
 * Sets default values if not provided in the options
 * 
 * @param {Chart} chartInstance 
 */
function setDefaults(chartInstance) {
    const defaults = { "backgroundColor": "rgba(52,132,210,0.2)", "borderColor": "rgba(52,132,210,0.6)", "snapToChart": false }

    Object.keys(defaults).forEach((field) => {
        if (typeof (chartInstance.options.plugins[pluginName][field]) === "undefined") {
            chartInstance.options.plugins[pluginName][field] = defaults[field]
        }
    })
}

/**
 * Checks if configuration contains all required fields
 * 
 * @param {Chart} chartInstance 
 */
function checkPluginConfiguration(chartInstance) {
    const requiredFields = { "onZoom": onZoom }

    if (!(pluginName in chartInstance.options.plugins)) {
        console.warn("Options are missing configuration for the '%s' plugin.", pluginName)
        chartInstance.options.plugins[pluginName] = {}
    }

    Object.keys(requiredFields).forEach((field) => {
        if (!(field in chartInstance.options.plugins[pluginName])) {
            console.warn("Plugin '%s' configuration is missing '%s', using default value.", pluginName, field)
            chartInstance.options.plugins[pluginName][field] = requiredFields[field]
        }
    })

    setDefaults(chartInstance)
}

/**
 * Checks if an event occurred in the chart boundaries
 *  
 * @param {MouseEvent} event 
 * @param {Chart} chartArea 
 */
function isEventInBoundaries(event, chart) {
    const boundingRect = chart.canvas.getBoundingClientRect()

    const x = event.clientX - boundingRect.left
    const y = event.clientY - boundingRect.top

    // NOTE: the Y axis is switched
    if (x < chart.chartArea.left || x > chart.chartArea.right ||
        y < chart.chartArea.top || y > chart.chartArea.bottom) {
        return false
    }

    return true
}

/**
 * Handler for mouse move event, if mouse button is clicked it draws a rectangle which represents selection
 *  
 * @param {MouseEvent} event 
 */
function onMouseMove(event) {
    const plugin = this[pluginName]

    if (!plugin.mouseDown || !isEventInBoundaries(event, this)) {
        return
    }

    const ctx = this.canvas.getContext("2d")
    const boundingRect = this.canvas.getBoundingClientRect()

    ctx.save()
    ctx.putImageData(plugin.imageData, 0, 0)

    const rectWidth = event.clientX - boundingRect.left - plugin.startPosition.x
    let rectHeight
    let startPositionY

    if (this.options.plugins[pluginName].snapToChart) {
        rectHeight = this.chartArea.bottom - this.chartArea.top
        startPositionY = this.chartArea.top
    } else {
        rectHeight = event.clientY - boundingRect.top - plugin.startPosition.y
        startPositionY = plugin.startPosition.y
    }

    ctx.beginPath()
    ctx.rect(plugin.startPosition.x, startPositionY, rectWidth, rectHeight)
    ctx.lineWidth = 1
    ctx.strokeStyle = this.options.plugins[pluginName].borderColor
    ctx.fillStyle = this.options.plugins[pluginName].backgroundColor
    ctx.fill()
    ctx.stroke()
    ctx.restore()
}

/**
 * Handler for mouse down, saves start coordinates of the event and a image of the chart canvas
 * 
 * @param {MouseEvent} event 
 */
function onMouseDown(event) {
    const plugin = this[pluginName]

    if (!isEventInBoundaries(event, this)) {
        return
    }

    const boundingRect = this.canvas.getBoundingClientRect()

    plugin.mouseDown = true
    plugin.startEvent = event
    plugin.startPosition = { x: event.clientX - boundingRect.left, y: event.clientY - boundingRect.top }
    plugin.imageData = this.canvas.getContext("2d").getImageData(0, 0, this.canvas.width, this.canvas.height)
}

/*
 * Handler for mouse up, saves selection end coordinates and restores chart image
 *  - If there was selection it will call the onZoom hook with the selection area dataD
 * 
 * @param {MouseEvent} event 
 */
function onMouseUp(event) {
    const plugin = this[pluginName]

    // If mouse is down it means we didn't start the drag inside the chart area
    if (!plugin.mouseDown) {
        return
    }

    const boundingRect = this.canvas.getBoundingClientRect()

    plugin.mouseDown = false
    plugin.endEvent = event
    plugin.endPosition = { x: event.clientX - boundingRect.left, y: event.clientY - boundingRect.top }

    this.canvas.getContext("2d").putImageData(plugin.imageData, 0, 0)

    if (plugin.startEvent.x != plugin.endEvent.x && plugin.startEvent.y != plugin.endEvent.y) {
        const xScale = Object.values(this.scales).find(scale => (scale.position === 'bottom' || scale.position === 'top'))
        const startValue = xScale.getValueForPixel(plugin.startPosition.x)
        const endValue = xScale.getValueForPixel(plugin.endPosition.x)

        this.options.plugins[pluginName].onZoom(startValue, endValue)
    }
}

const ZoomFetchPlugin = {
    id: pluginName,
    events: { "mousemove": onMouseMove, "mousedown": onMouseDown, "mouseup": onMouseUp },

    beforeInit: (chartInstance) => {
        checkPluginConfiguration(chartInstance)

        if (typeof (chartInstance[pluginName]) === "undefined") {
            chartInstance[pluginName] = { mouseDown: false }
        }
    },

    beforeEvent: (chartInstance, event) => {
        // Disable chart evenets when selection is active
        if (chartInstance[pluginName].mouseDown) {
            return false
        }
    },

    afterInit: (chartInstance) => {
        Object.keys(ZoomFetchPlugin.events).forEach((eventName) => {
            chartInstance.canvas.addEventListener(eventName, ZoomFetchPlugin.events[eventName].bind(chartInstance))
        })
    },
}

export default ZoomFetchPlugin 