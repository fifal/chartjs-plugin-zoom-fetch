## chartjs-plugin-zoom-fetch
### Installation
You can install the plugin using the npm:
```
npm install chartjs-plugin-zoom-fetch
```

### Usage
Example code on how to use this plugin:
```javascript
import Chart from 'chart.js'
import ZoomFetchPlugin from 'chartjs-plugin-zoom-fetch'

// Register plugin
Chart.plugins.register(ZoomFetchPlugin)

// Plugin options
const options = {
    plugins: {
        zoomFetch: {
            /** 
             * Callback that is called on zoom selection end
             * @param {Object} from: x-axis value from
             * @param {Object} to: x-axis value to
             */
            onZoom: function(from, to){
                // Implementation
            },

            // If set to true, the sellection height will always be same as the chart area height
            snapToChart: false
        }
    }
}

// Create Chart as usual
... 
```